# RTC sensor

Embedded devices like RPi do not have an onboard battery-backed clock. But devices can synchronize time with an Real-Time-Clock, so RTCs are typically attached to an I​2​C bus. The ​DS3231 and DS1307 has been chosen for this task