/*
 * main.cpp
 *
 *  Created on: 16 March 2019
 *      Author: blasm
 */
 
//#include <cstring>
#include <string>
//#include <iostream>

#include "RTC.cpp" /* Geany compiler force to use the *.cpp file 
					* instead of the *.h*/

using namespace std;

int main (){
	std::cout<<"Testing the C++ program\n";
	RTC mydev;
		
	// Write process
	mydev.setTime(10,1,10,1,8,8,91,0);
	// Read process
	mydev.getTime(NULL);
	
	/* Square Wave generation options[0-4] 
	 * for 1Hz(1),4Khz(2),8Khz(3) and 32kHz(4) and LED disabled with pull-up Res
	 * directly connected to SQ pin (0)
	 */
	mydev.setSqrSig(0);
	
	// Set alarm
	mydev.setAlarm(2,10,8,8,91); // Format min:hr --- date/month/year
	
	// Halt the Clock //
	mydev.haltTime();
	mydev.getTime(NULL);
	
	std::cout<<"End of C++ program\n";
	return 0;
}
