/*
 * RTC.h
 *
 *  Created on: 28 Feb 2019
 *      Author: blasm
 */

#ifndef SRC_RTC_H_
#define SRC_RTC_H_

#include <string>
using std::string;

class RTC {
private:
	int file;
	unsigned int i2c_port, address, bRegSize;	// Buffer size for DS1307 0x00 to 0x07 = 8
	//string date, time;
	virtual int bcd2Dec(char);
	virtual char dec2Bcd(int);
	virtual int openI2C();
	virtual void close();
	virtual int setRegAddrs(char,char);
	virtual char getCH();
	virtual void LED(string,string,string);
public:
	RTC(){file=0,i2c_port=1,address=0x68,bRegSize=8;}; //,date="",time="";};
	RTC(unsigned int, unsigned int, unsigned int);
	//RTC(string, string);
	virtual ~RTC();
	virtual char* getTime(char*);
	virtual int setTime(int,int,int,int,int,int,int,int);
	virtual int setAlarm(int,int,int,int,int);
	virtual int setSqrSig(int);
	virtual int haltTime();
};

#endif /* SRC_RTC_H_ */
