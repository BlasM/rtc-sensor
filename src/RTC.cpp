/*
 * RTC.cpp
 *
 *  Created on: 28 Feb 2019
 *      Author: blasm
 */

#include "RTC.h"
#include <iostream>	// In/Out methods
#include <iomanip>	// Width size delimiter setw()
#include <fcntl.h>
#include <unistd.h>	// Read & write methods
#include <stdint.h> // 1byte integer lib: uint8_t
#include <cstring>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <fstream>

#define FILE_LOC "/dev/i2c-1"
//#define GPIO "/sys/class/gpio/gpio17/"

using namespace std;

RTC::RTC(unsigned int i2c_port, unsigned int address, unsigned int bRegSize):
	file(0),i2c_port(i2c_port),address(address),bRegSize(bRegSize){}//,date(""),time(""){}
RTC::~RTC() {
	if(file!=-1) this->close();
	this->LED("/sys/class/gpio/gpio17/","value","0");
}
int RTC::bcd2Dec(char b){return((b/16)*10 + (b%16));}
char RTC::dec2Bcd(int b){return(((b/10)<<4)|(b%10));}

int RTC::openI2C(){
	// opening the I2C Bus
	std::cout<<"Starting Opening I2C port and device communication for DS1307\n";
	if ((file=open(FILE_LOC,O_RDWR)) <0 ){
		std::cout<<"Failed to open the I2C device file\n";
		return 1;}
	if (ioctl(file,I2C_SLAVE,address) < 0){
		std::cout<<"Failed to Connect to the sensor at "<<address<<"\n";
		return 1;} 
	return 0;
}
void RTC::close(){
	::close(file);
	file = -1;
}
/* method to set the Register location for Read or Write process 
 * values must be passed in HEX format : 0x00, 0x01 ... */
int RTC::setRegAddrs(char regloc,char val){
	char wBuff[2];		// Bus declaration
	wBuff[0]={regloc};	//Elements definition. Only works like this
	wBuff[1]={val};
	/* Set Buffer lenght to read if regloc=0x00h (buffer lenght = 1) or
	* to write if regloc != 0x00 (buffer length = 2)
	*/ 
	if (::write(file,wBuff,((val!=0x00)?2:1)) != ((val!=0x00)?2:1)){	
		std::cout<<"Problem setting Register address \n";
		return 1;}
	return 0;
}
char* RTC::getTime(char* time){
	this->openI2C();
	this->setRegAddrs(0x00,0x00); // Setting location reading pointer 
	std::cout<<"Starting Reading process\n";
	bRegSize = 7; // Number of registers to read
	char bufRd[bRegSize]={0x00};
	if (::read(file,bufRd,bRegSize)!=(int)bRegSize){
		std::cout<<"Problems reading the Registers from the I2C device\n";
		return NULL;
	} else {
		//std::cout<<"Data read: "<< setw(bRegSize)<<bufRd<<"\n";
		std::cout<<"Converted Date: "<<this->bcd2Dec(bufRd[4])<<"/"<<this->bcd2Dec(bufRd[5])<<"/"<<this->bcd2Dec(bufRd[6])<<"\t"<<"week-day: "<<this->bcd2Dec(bufRd[3])<<"\n";
		std::cout<<"Converted Time: "<<this->bcd2Dec(bufRd[2])<<":"<<this->bcd2Dec(bufRd[1])<<":"<<this->bcd2Dec(bufRd[0])<<"\n";
		time = &bufRd[0];
	}
	this->close();
	return time;
}
/* Returns the byte containing the CH bit */
char RTC::getCH(){
	this->setRegAddrs(0x00,0x00); // reg 0x00 ; keep clok halt = 0x80
	char rbyte[1];
	if (::read(file,rbyte,1) != 1){
		std::cout<<"Problem Reading Register 0x00 containing CH bit \n";
		return 1;}
	return rbyte[0];
}
/* Set time in the RTC by passing each value as integer. Each integer 
 * represents a date/time value. Same method is used to set the Alarm
 * in a different memory location, int alarm = 1
 */
int RTC::setTime(int sec,int min,int hr,int day,int date,int mnth,int year, int alarm){	//(string date, string time){
	char bufWrt[8];	// 8 registers required to set time from 0x00 to 0x07
	if (alarm >= 1){
		bufWrt[0]= 0x10;	// Alarm 1st Reg loc in 0x10
		std::cout<<"Starting Setting Alarm process\n";
	} else {
		bufWrt[0]= 0x00;	// 1st reg location for RTC Time
		std::cout<<"Starting Writing process\n";
	}
	bufWrt[1]=dec2Bcd(sec);
	bufWrt[2]=dec2Bcd(min);
	bufWrt[3]=dec2Bcd(hr);
	bufWrt[4]=dec2Bcd(day);
	bufWrt[5]=dec2Bcd(date);
	bufWrt[6]=dec2Bcd(mnth);
	bufWrt[7]=dec2Bcd(year);
	this->openI2C();
	if (::write(file,bufWrt,8) != 8){
		std::cout<<"Problems writing the Registers of the I2C device\n";
		return 1;}
	this->close();
	return 0;
}
/* Turn On/OFF GPIO from RPi */
void RTC::LED(string path, string filename, string value){
	ofstream fs;
	fs.open((path+filename).c_str());
	fs<<value;
	fs.close();
}
/* Set Alarm at register 0x10 with same format than RTC time
 * Loops waiting for the alarm to trigger by redaing every 5 sec 
 * the RTC time. When Time matches or passes Alarm settings, 
 * it shows a message on screen and swhicht ON a LED at GPIO 17
 */
int RTC::setAlarm(int min,int hr,int date,int mnth,int year){
	// Set Alarm TIme in BCD format
	this->setTime(0,min,hr,0,date,mnth,year,1);
	bRegSize = 7;
	char bufRd[bRegSize]={0x00};
	bool flag = true;	// Alarm event matching RTC time 
	do {std::cout<<"Waiting for Alarm ... ZZZZZZ.... ZZZZZ .....\n";
		this->openI2C();
		this->setRegAddrs(0x00,0x00);
		if (::read(file,bufRd,bRegSize)!=(int)bRegSize){
			std::cout<<"Problems reading the Registers from the I2C device\n";
			return 1;}
		// Compares Date first // 
		if ((bcd2Dec(bufRd[6]) == year) && (bcd2Dec(bufRd[5]) == (char)mnth) && (bcd2Dec(bufRd[4]) == (char)date)) {
			// Compares Time with Hours and minutes //
			if ((bcd2Dec(bufRd[2]) >= (char)hr) && (bcd2Dec(bufRd[1]) >= (char)min)) {
				std::cout<<"Alarm Triggered!! \n";
				flag = false;
				this->LED("/sys/class/gpio/gpio17/","value","1");
			}
		}
		this->close();
		sleep(5);		// waits for 5 seconds before checking again
	}while(flag);
	return 0;
}
int RTC::setSqrSig(int opt){
	char bufWrt[2];
	bufWrt[0]= 0x07;	// Ctrl reg location		
	switch (opt){
		case 0:
			std::cout<<"Disable Square Signal\n";
			bufWrt[1]={0x80};	//1000 0000b for testing LED with pull-up Res directly connected
			break;
		case 1:
			std::cout<<"Selected Freq 1Hz\n";
			bufWrt[1]={0x10};	//0001 0000b
			break;
		case 2:
			std::cout<<"Selected Freq 4kHz\n";
			bufWrt[1]={0x11};	//0001 0001b
			break;
		case 3:
			std::cout<<"Selected Freq 8kHz\n";
			bufWrt[1]={0x12};	//0001 0010b
			break;
		case 4:
			std::cout<<" Selected Freq 32kHz\n";
			bufWrt[1]={0x13};	//0001 0011b
			break;
		default :
			std::cout<<"No valid option selected, please choose from 1-4\n";
			break;
	}
	this->openI2C();
	if (::write(file,bufWrt,2) != 2){
		std::cout<<"Problems writing the Registers of the I2C device\n";
		return 1;}
	this->close();	
	return 0;
}
int RTC::haltTime(){
	this->openI2C();
	// Checking CH bit for setting time 
	char bCH = this->getCH();
	std::cout<<"CH byte returned: "<< (int)bCH <<"\n";
	char mask = bCH | (char)0x80;
	std::cout<<"CH byte after mask: "<< int(mask) <<"\n";
	this->setRegAddrs(0x00,mask); // reg 0x00 ; keep clok halt = 0x80
	this->close();
	return 0;
}


/*int main (){
	std::cout<<"Testing the C++ program\n";
	RTC mydev;
		
	// Write process
	mydev.setTime(10,1,10,1,8,8,91,0);
	// Read process
	mydev.getTime(NULL);
	
	// Square Wave generation options[0-4] 
	// for 1Hz(1),4Khz(2),8Khz(3) and 32kHz(4) and LED disabled with pull-up Res
	// directly connected to SQ pin (0)
	 //
	mydev.setSqrSig(0);
	
	// Set alarm
	mydev.setAlarm(2,10,8,8,91); // Format min:hr --- date/month/year
	
	// Halt the Clock //
	mydev.haltTime();
	mydev.getTime(NULL);
	
	std::cout<<"End of C++ program\n";
	return 0;
}
*/
